#ifndef ANN_TWO_LAYER_NETWROK_H
#define ANN_TWO_LAYER_NETWROK_H

#include "matrix.h"

namespace ann
{

class two_layer_network
{
  // - - - type definitions - - - - - - - - - - - - - - - - - - - - - - - - - - -

  public:
    typedef ann::matrix::real_t real_t;
    typedef ann::matrix::size_t size_t;

  // - - - member variables - - - - - - - - - - - - - - - - - - - - - - - - - - -

  private:
    /// Learning rate
    real_t eta_m;
    /// First layer weights
    matrix w1_m;
    /// Second layer weights
    matrix w2_m;

    /// The second layer activities (z_m) are only stored to speed up computation.
    /// They are mutable to allow manipulating them in const functions (like
    /// activate).
    mutable matrix zm_m;

  // - - - methods  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  public:
    /// Creates a two layer network with M neurons (+1 bias neuron) in the
    /// hidden layer.
    two_layer_network (size_t M_a, size_t D_a ,real_t eta_a = 0.01);

    /// Initializes the networks weights with random values from the given interval.
    void initialize_weights (real_t lower_a, real_t upper_a);

    /// Returns the learning rate.
    inline real_t eta () const { return eta_m; }
    /// Sets the learning rate to the given value.
    inline void set_eta (real_t eta_a) { eta_m = eta_a; }

    /// Returns the number of hidden neurons M (excluding the bias neuron).
    inline size_t M () const { return w1_m.rows(); }

    /// Activates the network: Computes network output y for a given input x.
    real_t activate (const matrix& x_a) const;
    /// Calls activate.
    inline real_t operator() (const matrix& x_a) const { return activate(x_a); }

    /// Trains the network with the given input and target output; i.e. does one
    /// step of gradient descent for the given sample. In yet other words, this
    /// implements error backpropagation.
    void train (const matrix& x_a, real_t t_a);

    /// Trains the network with multiple training samples (an epoch).
    /// Each row of the given matrix is expected to contain input x and target
    /// output t.
    void train_epoch (const std::vector<std::pair<matrix,real_t>>& samples_a);

  private:
    /// The activation function f_act used in the hidden layer
    real_t activation_function (real_t value_a) const;
};

}

#endif //ndef ANN_TWO_LAYER_NETWROK_H
