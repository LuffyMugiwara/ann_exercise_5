/* Programming Exercise 5

   Please leave your names here.
   author(s):

   108012209500 Benjamin Lentz
   108012234853 Emanuel Durmaz

 d)
 Es wurden für alle Werte von D jeweils die Werte M = 2,10,30,60 getestet. Die Auführung wurde fünf mal wiederholt, um die Auswirkung des Zufalls auf die Prozedur besser einschätzen zu können.
 Beim Training des Neuronalen Netzwerkes sank der Fehler stetig mit steigender Anzahl von Epochen.
 Die Testfehler hingegen haben meist ein Minimum bei den ersten tausend Epochen. Danach steigt der Fehler wieder aber pendelt sich bei einem bestimmten Wert ein. Dies ist wahrscheinlich auf die Überanpassung des Netzwerkes auf die Trainingsmenge zurück zu führen.
 Der am Ende des Trainings erreichte Fehlerwert wurde über die verschiedenen Ausführungen gemittelt und zur Bewertung der Anpassung herangezogen, ob sie erfolgreich war oder nicht.
 Nur bei D=10 konnte dieser Wert mit einer Anzahl von M = 2 verdeckten Neuronen erreicht werden.

*/

#include "two_layer_network.h"
#include "matrix.h"

#include <cmath>
#include <fstream>
#include <iostream>
#include <string>

// to use random_shuffle
#include <algorithm>

// @hint: Use shorthands to commonly used types.
typedef ann::matrix matrix;
typedef ann::two_layer_network network;
typedef network::real_t real_t;
typedef network::size_t size_t;
typedef std::string string;

//declare functions
void write_data (const string& filename_a, const matrix& data_a);

// Returns the square of the given value.
real_t square (real_t value_a)
{
  return value_a * value_a;
}

/// Writes content of a matrix to file in a plottable format.
void write_data (const string& filename_a, const matrix& data_a)
{
  std::ofstream out_l(filename_a);
  for (size_t row_l = 0; row_l < data_a.rows(); ++row_l)
  {
    for (size_t col_l = 0; col_l < data_a.cols() - 1; ++col_l)
    {
      out_l << data_a(row_l, col_l) << '\t';
    }
    out_l << data_a(row_l, data_a.cols() - 1) << std::endl;
  }
}

/// Returns the MSSE of the network on the samples.
/// Each row of the given matrix is expected to contain input x and target
/// output t.
real_t compute_error (const std::vector<std::pair<matrix,real_t>>& samples_a, const network& net_a)
{
  //Compute the MSSE
    real_t P = samples_a.size();
    real_t sum = 0.0;

    for(size_t p=0; p < P; p++)
    {
        matrix x_p = samples_a[p].first;
        real_t t_p = samples_a[p].second;
        real_t y = net_a(x_p); //same like net_a.activate(x_p)

        sum = sum + square(y - t_p);
    }

    return (1.0/P) * sum;
}


//create target value
real_t f_function(const matrix& x_p)
{
    size_t D = x_p.rows();

    size_t sum = 0;
    for(size_t d=1; d <= D; d++)
    {
        sum = (size_t) (sum + x_p(0,d));
    }
    sum = sum % 2;

    real_t t_p;
    if(sum == 0)
        t_p = -1;
    else
        t_p = 1;

    return t_p;
}


void initialize_sets(size_t D, std::vector<std::pair<matrix,real_t>>& training_set, std::vector<std::pair<matrix,real_t>>& testing_set)
{
    //create training set
    size_t P = std::pow(2,D); // 2^D possible values
    std::vector<matrix> input_vectors(P);

    //create input values
    for(size_t p = 0; p < P; p++)
    {
        matrix x_p(1,D+1);
        x_p(0,0) = 1; //bias
        for(size_t d=1; d <= D; d++)
        {
            size_t x_pd = (size_t)(std::floor(p / std::pow(2, d-1))) % 2;
            x_p(0,d) = x_pd;
        }
        input_vectors[p] = x_p;
    }

    //randomize the order
    std::random_device rand_dev;
    std::mt19937 gen(rand_dev());
    std::shuffle(input_vectors.begin(), input_vectors.end(), gen);

//    std::cout << std::endl;
//    for(size_t p = 0; p < P/2; p++)
//        std::cout << p << " ";
//    std::cout << std::endl;
//
//    for(size_t p = (P/2); p < P; p++)
//        std::cout << p << " ";
//    std::cout << std::endl;

    //create training set
    training_set.resize(P);
    for(size_t p = 0; p < P/2; p++)
    {
        matrix x_p = input_vectors[p];
        real_t t_p = f_function(x_p);

        std::pair<matrix,real_t> pair_p = std::make_pair(x_p,t_p);
        training_set[p] = pair_p;
    }

    //create testing set
    testing_set.resize(P);
    for(size_t p = (P/2); p < P; p++)
    {
        matrix x_p = input_vectors[p];
        real_t t_p = f_function(x_p);

        std::pair<matrix,real_t> pair_p = std::make_pair(x_p,t_p);
        training_set[p] = pair_p;
    }
}

int main (int argc, char** argv)
{
    // Some more shorthands that might be useful.
    using std::exp;
    using std::sin;
    using std::string;
    using std::to_string;
    using std::ofstream;
    using std::cout;
    using std::endl;

    //configuration
    size_t M[] = {2, 10, 30, 60};
    size_t M_len = sizeof(M) / sizeof(*M);

    size_t D[] = {2, 5, 8, 10};
    size_t D_len = sizeof(D) / sizeof(*D);

    size_t num_of_training_runs = 5;

    real_t eta = 0.01;
    real_t interval_min = -2.0;
    real_t interval_max = 2.0;

    size_t num_of_epochs = 10000;

    for(size_t h=0; h < D_len; h++) //iterate over D
    {
        std::cout << "D: " << D[h] << std::endl;

        //Create training and testing samples
        std::vector<std::pair<matrix,real_t>> training_set(0);
        std::vector<std::pair<matrix,real_t>> testing_set(0);
        initialize_sets(D[h],training_set,testing_set);

        //print training set
        //for(size_t i=0; i < training_set.size(); i++)
        //    std::cout << training_set[i].first  << "\n" << training_set[i].second;

        for(size_t i=0; i < M_len; i++) //iterate over M
        {
            std::cout << " M: " << M[i] << std::endl;

            for(size_t j=1; j <= num_of_training_runs; j++)
            {
                std::cout << "  " << j << ". run" << std::endl;

                network netw(M[i],D[h], eta);
                matrix msse_train(num_of_epochs, 2);
                matrix msse_test(num_of_epochs, 2);
                netw.initialize_weights(interval_min, interval_max); //initialize new weights for every new training run

                //training
                for (size_t k = 0; k < num_of_epochs; k++)
                {
                    netw.train_epoch(training_set);
                    real_t error_train = compute_error(training_set, netw);
                    real_t error_test = compute_error(testing_set, netw);

                    //save error value
                    msse_train(k, 0) = k; //index of epoch
                    msse_train(k, 1) = error_train;

                    msse_test(k, 0) = k; //index of epoch
                    msse_test(k, 1) = error_test;
                }

                //save to files
                //sample_network("netw_" +std::to_string(M[i]) + "_" + std::to_string(j) + ".txt", netw, -7.5, 7.5, 1000);
                write_data("error_train_D" + std::to_string(D[h]) + "_M" + std::to_string(M[i]) + "_" + std::to_string(j) + ".txt", msse_train);
                write_data( "error_test_D" + std::to_string(D[h]) + "_M" + std::to_string(M[i]) + "_" + std::to_string(j) + ".txt", msse_test);
            }
        }
    }


    return 0;
}
