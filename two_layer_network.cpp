/*
 * Programming Exercise 5
 * 108012209500 Benjamin Lentz
 * 108012234853 Emanuel Durmaz
 */

#include "two_layer_network.h"

#include <cassert>


/// Creates a two layer network with M neurons (+1 bias neuron) in the
/// hidden layer.
ann::two_layer_network::two_layer_network (size_t M_a, size_t D_a, real_t eta_a)
: eta_m(eta_a),
  w1_m(M_a, D_a+1), // return M_a with M() , m=1...M -> |M| , d=0...D -> |D+1|
  w2_m(1, M_a+1),
  zm_m(M_a+1, 1, 1) //initialize with ones , zm_m with m= 0...M -> |M+1|
{


}


/// Initializes the networks weights with random values from the given interval.
void ann::two_layer_network::initialize_weights (real_t lower_a, real_t upper_a)
{
    //Initialize all weights randomly.
    w1_m.fill_with_uniform_samples(lower_a,upper_a);
    w2_m.fill_with_uniform_samples(lower_a,upper_a);
}


/// Activates the network: Computes network output y for a given input x.
ann::two_layer_network::real_t ann::two_layer_network::activate (const matrix& x_a) const
{
    //calculate zm_m
    for(size_t m=1; m < zm_m.rows(); m++) // <= M()
    {
        real_t a = 0;
        for(size_t d=0; d < x_a.cols(); d++)
        {
            a = a + w1_m(m-1,d)* x_a(0,d);
        }

        zm_m(m,0) = activation_function(a);
    }

    //calculate y
    real_t sum = 0.0;
    for(size_t m=1; m < zm_m.rows(); m++) // <= M()
    {
        sum = sum + ( w2_m(0,m)  * zm_m(m,0) );
    }
    real_t y = w2_m(0,0) + sum;

    return y;
}


// Trains the network with the given input and target output; i.e. does one
// step of gradient descent for the given sample. In yet other words, this
// implements error backpropagation.
void ann::two_layer_network::train (const matrix& x_a, real_t t_a)
{
    //Train the network
    real_t y = activate(x_a);
    real_t delta = y - t_a;

    matrix delta_m(M()+1,1,1);

    //calculate deltas
    for (size_t m = 0; m <= M(); m++)
    {
        delta_m(m,0) = zm_m(m,0) * (1 - zm_m(m,0) ) * delta * w2_m(0,m);
    }

    //one step of gradient descent
    w2_m(0,0) = w2_m(0,0) + ( -eta_m * delta * 1.0 );

    for (size_t m = 1; m <= M(); m++) //skip m = 0
    {
        w2_m(0,m) = w2_m(0,m) + ( -eta_m * delta * zm_m(m,0) );

        for(size_t d=0; d < x_a.cols(); d++)
        {
            w1_m(m-1,d) = w1_m(m-1,d) + ( -eta_m * delta_m(m,0) * x_a(0,d));
        }
    }
}


/// Trains the network with multiple training samples (an epoch).
/// Each row of the given matrix is expected to contain input x and target
/// output t.
void ann::two_layer_network::train_epoch (const std::vector<std::pair<matrix,real_t>>& samples_a)
{
    //Train the network with each of the given samples.
    for(size_t p = 0; p < samples_a.size(); p++)
    {
        matrix x_p = samples_a[p].first;
        real_t t_p = samples_a[p].second;
        train(x_p,t_p);
    }
}


/// The activation function f_act used in the hidden layer
ann::two_layer_network::real_t ann::two_layer_network::activation_function (real_t value_a) const
{
    //the activation function
    return 1.0 / (1.0 + std::exp(-value_a) );
}

